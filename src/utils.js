
const { parse } = require('url');
const { notFound } = require('./routes/handlers/error')
const fs = require('fs')
const path = require('path')

const trimPath = ({ pathname }) => pathname.replace(/^\/+|\+$/g, '');

const getPath = url => trimPath(parse(url, true))

const getQuery = url => parse(url, true).query

const isNumber = n => Number(n) === n

const isObject = obj => obj === Object(obj)

const validateObject = obj => (isObject(obj) ? obj : {})

const readFileSync = dir => fs.readFileSync(path.resolve(__dirname, dir))

const getHandler = ({ routes, method, path }) => {
    method = method.toLowerCase()

    if (method in routes) {
        const actions = routes[method];
        return path in actions ? actions[path] : notFound;
    } else {
        return notFound
    }
};

module.exports = {
    getPath,
    getQuery,
    isNumber,
    isObject,
    validateObject,
    readFileSync,
    getHandler
}