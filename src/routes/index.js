const post = require('./handlers/post');
const get = require('./handlers/get');

module.exports = {
    post,
    get
};