module.exports = {
    sample: (data, callback = () => {}) => {
        callback(200, data.payload)
    },
    vote: ({ payload }, io, callback = () => {}) => {
        console.log(payload)
        io.emit("publish-vote", payload);

        callback(200, {ok: true})
    }
};
