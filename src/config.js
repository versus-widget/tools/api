let environment = {}

environment.staging = {
    'httpPort' : 3000,
    'httpsPort': 3001,
    'envName': 'staging',
}

environment.production = {
    'httpPort' : 5000,
    'httpsPort': 5001,
    'envName': 'production',
}

// Determines which env was passes as a command line argument
const currentEnv = process?.env?.NODE_ENV || ''
const envExport = currentEnv in environment 
                    ? environment[currentEnv]
                    : environment.staging

module.exports = envExport