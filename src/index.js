// Dependencies
const { createServer } = require("http");
const { Server } = require("socket.io")
const routes = require('./routes');
const { StringDecoder } = require('string_decoder')
const config = require('./config')

const {
    getPath,
    getQuery,
    isNumber,
    isObject,
    // validateObject,
    getHandler,
    readFileSync
} = require('./utils');

const { httpPort, httpsPort, envName } = config;
const decoder = new StringDecoder('utf-8');

const serverHandler = (req, res) => {
    const { url, method, headers } = req;

    // Get the URL and parse it
    const path  = getPath(url);
    const query = getQuery(url);
    let buffer = '';

    // Get the payload
    req.on('data', (chunk) => { buffer += decoder.write(chunk) });
    req.on('end', () => {
        buffer += decoder.end()
        
        const handler = getHandler({
            routes,
            method,
            path
        });

        const data = {
            path,
            query,
            method,
            headers,
            // payload: validateObject(buffer)
            payload: JSON.parse(buffer)
        }

        handler(data, io, (statusCode,  data) => {
            const status = isNumber(statusCode) ? statusCode : 200;
            const payload = isObject(data) ? JSON.stringify(data) : JSON.stringify({});

            res.setHeader('Content-Type', 'application/json')
            res.writeHead(status);
            res.end(payload);
        });
    });
};


const httpServerIO = createServer(serverHandler)
const io = new Server(httpServerIO, {
    cors: {
        origin: 'http://localhost/5173',
        methods: ["GET", "POST"],
    }
});

io.on("connection", socket => { 
    console.log('user connected')
    io.on("broadcast", () => console.log('listen broadcast'))
});

httpServerIO.listen(httpPort, () => {
    console.log(`HTTP server is listening on port ${httpPort} using ${envName} mode using socket`)
});