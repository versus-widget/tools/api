# Vs Widget API

This api will listen the commands triggered from the [google meet bot](https://gitlab.com/versus-widget/tools/chat-bot)

Works on: `localhost:3000/`

### Usage:

POST: `http://localhost:3000/vote`

```json
{
    "user": "username",
    "command": "/go class"
}
```

**username**: the bot will pick the username from the chat box

**command**: there are two `/go class` and `/go functional`
